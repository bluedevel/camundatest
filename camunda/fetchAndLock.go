package camunda

import (
	"errors"
)

func (client CamundaClient) FetchAndLock(workerId, topic string) (*Task, error) {
	request := FetchAndLockRequest{
		WorkerId: workerId,
		MaxTasks: 1,
		Topics: []Topic{
			{TopicName: topic, LockDuration: 5000},
		},
	}

	tasks := make([]Task, 0)
	err := client.PostJson("/external-task/fetchAndLock", &request, &tasks)
	if err != nil {
		return nil, err
	}

	if len(tasks) == 0 {
		return nil, errors.New("no tasks received")
	}

	task := tasks[0]
	return &task, nil
}
