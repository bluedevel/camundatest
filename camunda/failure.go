package camunda

func (client CamundaClient) Failure(task Task, message string, retryTimeout int) error {
	retries := 0
	if task.Retries != nil && *task.Retries > retries {
		retries = *task.Retries - 1
	}

	request := FailureRequest{
		WorkerId:     task.WorkerId,
		ErrorMessage: message,
		Retries:      retries,
		RetryTimeout: retryTimeout,
	}

	err := client.PostJson("/external-task/"+task.Id+"/failure", &request, nil)

	if err != nil {
		return err
	}

	return nil
}
