package camunda

import (
	"net/http"
	"encoding/json"
	"bytes"
	"fmt"
)

type CamundaClient struct {
	baseUrl string
}

func New(baseUrl string) CamundaClient {
	if baseUrl[len(baseUrl)-1] == '/' {
		baseUrl = baseUrl[:len(baseUrl)-1]
	}

	return CamundaClient{baseUrl}
}

func (client CamundaClient) GetJson(path string, data interface{}) error {
	resp, err := http.Get(client.baseUrl + path)
	if err != nil {
		return err
	}

	decoder := json.NewDecoder(resp.Body)

	err = decoder.Decode(data)
	if err != nil {
		return err
	}

	return nil
}

func (client CamundaClient) PostJson(path string, reqData, resData interface{}) error {
	buffer := bytes.Buffer{}
	encoder := json.NewEncoder(&buffer)
	err := encoder.Encode(reqData)

	if err != nil {
		return err
	}

	resp, err := http.Post(client.baseUrl+path, "application/json", &buffer)
	if err != nil {
		return err
	}

	body := bytes.Buffer{}
	body.ReadFrom(resp.Body)

	if !(200 <= resp.StatusCode && resp.StatusCode < 300) {
		return fmt.Errorf("http status %s: %s", resp.Status, body.String())
	}

	if body.String() == "" {
		return nil
	}

	bodyBuf := body.String()
	decoder := json.NewDecoder(&body)

	err = decoder.Decode(resData)
	if err != nil {
		return fmt.Errorf("%s %s", err, bodyBuf)
	}

	return nil
}
