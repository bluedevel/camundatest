package camunda

func (client CamundaClient) BpmnError(task Task, errorCode string) error {
	request := BpmnErrorRequest{
		WorkerId:  task.WorkerId,
		ErrorCode: errorCode,
	}

	err := client.PostJson("/external-task/"+task.Id+"/bpmnError", &request, nil)

	if err != nil {
		return err
	}

	return nil
}
