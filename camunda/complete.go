package camunda

func (client CamundaClient) Complete(task Task, variables interface{}) error {
	request := CompleteRequest{
		task.WorkerId,
		variables,
	}

	err := client.PostJson("/external-task/"+task.Id+"/complete", &request, nil)
	if err != nil {
		return err
	}

	return nil
}
