package camunda

type Task struct {
	Id         string              `json:"id"`
	ActivityId string              `json:"activityId"`
	WorkerId   string              `json:"workerId"`
	Retries    *int                `json:"retries"`
	Variables  map[string]Variable `json:"variables"`
}

type Variable struct {
	Value string `json:"value"`
	Type  string `json:"type"`
}

type FetchAndLockRequest struct {
	WorkerId string  `json:"workerId"`
	MaxTasks int     `json:"maxTasks"`
	Topics   []Topic `json:"topics"`
}

type Topic struct {
	TopicName    string `json:"topicName"`
	LockDuration int    `json:"lockDuration"`
}

type CompleteRequest struct {
	WorkerId  string      `json:"workerId"`
	Variables interface{} `json:"variables"`
}

type FailureRequest struct {
	WorkerId     string `json:"workerId"`
	ErrorMessage string `json:"errorMessage"`
	Retries      int    `json:"retries"`
	RetryTimeout int    `json:"retryTimeout"`
}

type BpmnErrorRequest struct {
	WorkerId  string `json:"workerId"`
	ErrorCode string `json:"errorCode"`
}
