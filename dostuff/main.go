package main

import (
	"log"
	"time"
	"bitbucket.org/bluedevel/camundatest/camunda"
)

const workerId = "MrRobot"

func main() {
	client := camunda.New("http://localhost:8080/engine-rest/")

	for {
		task, err := client.FetchAndLock(workerId)
		if err != nil {
			log.Println(err)
			time.Sleep(1 * time.Second)
			continue
		}

		log.Printf("Fetched task %s in activity %s\n", task.Id, task.ActivityId)

		if processVariable, ok := task.Variables["important"]; ok {
			log.Printf("Processed task %s with variable %s", task.Id, processVariable.Value)
			err = client.Complete(*task, nil)
		} else {
			if task.Retries == nil {
				task.Retries = new(int)
				*task.Retries = 5
			}

			err = client.Failure(*task, "missing process variable", 2000)
		}

		if err != nil {
			log.Println(err)
			time.Sleep(1 * time.Second)
			continue
		}
	}
}
