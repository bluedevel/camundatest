package main

import (
	"bitbucket.org/bluedevel/camundatest/camunda"
	"log"
	"time"
	"strings"
	"errors"
)

const (
	Calzone   = "calzone"
	Margarita = "margarita"
	Salami    = "salami"
)

var knownPizzas = []string{Calzone, Margarita, Salami}

var ingredients = map[string]string{
	Calzone:   "cheese,tomato",
	Margarita: "tomato",
	Salami:    "tomato,salami",
}

func main() {
	client := camunda.New("http://localhost:8080/engine-rest/")

	for {
		task, err := client.FetchAndLock("check_ingredients_worker", "check_ingredients")
		if err != nil {
			log.Println(err)
			time.Sleep(5 * time.Second)
			continue
		}

		if pizza, ok := task.Variables["pizza"]; ok && pizzaIsKnown(pizza.Value) {
			ingr := ingredients[pizza.Value]

			err := checkIngredients(ingr)
			if err != nil {
				if task.Retries == nil {
					task.Retries = new(int)
					*task.Retries = 3
				}
				log.Println(err)
				client.BpmnError(*task, "missing_incredients")
				continue
			}

			log.Println("Checked ingredients for " + pizza.Value)
			err = client.Complete(*task, map[string]camunda.Variable{"ingredients": {Value: ingr, Type: "String"}})
			if err != nil {
				log.Println(err)
			}
		} else {
			log.Println("Missing ingredients")
			err = client.Failure(*task, "Missing pizza type or bad type of variable", 0)
			if err != nil {
				log.Println(err)
			}
		}
	}

}

func checkIngredients(ingr string) (error) {
	if strings.Contains(ingr, "cheese") {
		return errors.New("no cheese for you corleone")
	}
	return nil
}

func pizzaIsKnown(pizza string) bool {
	for _, known_pizza := range knownPizzas {
		if pizza == known_pizza {
			return true
		}
	}

	return false
}
