package main

import (
	"bitbucket.org/bluedevel/camundatest/camunda"
	"log"
	"time"
)

func main() {
	client := camunda.New("http://localhost:8080/engine-rest/")

	for {
		task, err := client.FetchAndLock("order_entry_worker", "invoice_entry")
		if err != nil {
			log.Println(err)
			time.Sleep(5 * time.Second)
			continue
		}

		if address, ok := task.Variables["adresse"]; ok && address.Type == "String" {
			sendConfirmation(address.Value)
			err = client.Complete(*task, nil)
			if err != nil {
				log.Println(err)
				continue
			}
		} else {
			err = client.Failure(*task, "Missing address or bad type of variable", 0)
			if err != nil {
				log.Println(err)
				continue
			}
		}
	}

}

func sendConfirmation(address string) {
	log.Println("Sending confirmation for address " + address)
}
