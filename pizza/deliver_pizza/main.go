package main

import (
	"bitbucket.org/bluedevel/camundatest/camunda"
	"log"
	"time"
)

func main() {
	client := camunda.New("http://localhost:8080/engine-rest/")

	for {
		task, err := client.FetchAndLock("deliver_pizza_worker", "deliver_pizza")
		if err != nil {
			log.Println(err)
			time.Sleep(5 * time.Second)
			continue
		}

		if address, ok := task.Variables["adresse"]; ok && address.Type == "String" {
			deliverPizza(address.Value)
			err = client.Complete(*task, nil)
			if err != nil {
				log.Println(err)
				continue
			}
		} else {
			err = client.Failure(*task, "Missing address or bad type of variable", 0)
			if err != nil {
				log.Println(err)
				continue
			}
		}
	}

}

func deliverPizza(address string) {
	log.Println("Deliver pizza to address " + address)
	time.Sleep(4 * time.Second)
}
