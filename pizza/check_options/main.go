package main

import (
	"bitbucket.org/bluedevel/camundatest/camunda"
	"log"
	"time"
)

func main() {
	client := camunda.New("http://localhost:8080/engine-rest/")

	for {
		task, err := client.FetchAndLock("check_options_worker", "check_options")
		if err != nil {
			log.Println(err)
			time.Sleep(5 * time.Second)
			continue
		}

		if options, ok := task.Variables["options"]; ok {
			log.Println("Checking if " + options.Value + " is in stock")
			err = client.Complete(*task, nil)
			if err != nil {
				log.Println(err)
				continue
			}
		} else {
			err = client.Failure(*task, "no options variable found", 0)
			if err != nil {
				log.Println(err)
				continue
			}
		}
	}

}
