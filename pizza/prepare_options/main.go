package main

import (
	"bitbucket.org/bluedevel/camundatest/camunda"
	"log"
	"time"
	"math/rand"
	"strconv"
)

func main() {
	client := camunda.New("http://localhost:8080/engine-rest/")

	for {
		task, err := client.FetchAndLock("prepare_options_worker", "prepare_options")
		if err != nil {
			log.Println(err)
			time.Sleep(5 * time.Second)
			continue
		}

		if options, ok := task.Variables["options"]; ok {
			log.Println("Preparing " + options.Value)
			time.Sleep(1 * time.Second)
			err = client.Complete(*task, map[string]camunda.Variable{
				"optionsRef": {
					Value: strconv.Itoa(random(1000, 9999)),
					Type:  "String",
				},
			})
			if err != nil {
				log.Println(err)
				continue
			}
		} else {
			err = client.Failure(*task, "no options variable found", 0)
			if err != nil {
				log.Println(err)
				continue
			}
		}
	}

}

func random(min, max int) int {
	rand.Seed(time.Now().Unix())
	return rand.Intn(max-min) + min
}
