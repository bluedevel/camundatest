package main

import (
	"bitbucket.org/bluedevel/camundatest/camunda"
	"log"
	"time"
	"math/rand"
	"strconv"
)

func main() {
	client := camunda.New("http://localhost:8080/engine-rest/")

	for {
		task, err := client.FetchAndLock("pizza_maker", "make_pizza")
		if err != nil {
			log.Println(err)
			time.Sleep(5 * time.Second)
			continue
		}

		if ingredients, ok := task.Variables["ingredients"]; ok && ingredients.Type == "String" {
			log.Println("Making pizza with " + ingredients.Value)
			time.Sleep(3 * time.Second)
			err = client.Complete(*task, map[string]camunda.Variable{"pizzaRef":
			{
				Value: strconv.Itoa(random(1000, 9999)),
				Type:  "String",
			}})
			if err != nil {
				log.Println(err)
				continue
			}
		} else {
			log.Println("Missing ingredients")
			err = client.Failure(*task, "Missing ingredients", 0)
			if err != nil {
				log.Println(err)
				continue
			}
		}
	}
}

func random(min, max int) int {
	rand.Seed(time.Now().Unix())
	return rand.Intn(max-min) + min
}
